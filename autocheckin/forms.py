import logging

import coloredlogs
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from phonenumbers import parse, is_possible_number, NumberParseException
from validate_email import validate_email

import southwestwatchdog.django_settings as s

logger = logging.getLogger(__name__)
coloredlogs.install(level=s.LOG_LEVEL,
                    fmt=s.LOG_FORMAT)


class AutoCheckinForm(forms.Form):
    SAVINGS_SIGNUP_OPTIONS = (('YES', 'YES'), ('NO', 'NO'))
    DELIVERY_OPTIONS = (('EMAIL', 'EMAIL'), ('TEXT', 'TEXT'))
    confirmation_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                                        'required': True,
                                                                        'maxlength': 6,
                                                                        'placeholder': "DBZ0FG"}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                               'required': True,
                                                               'maxlength': 20,
                                                               'placeholder': 'Jane'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                              'required': True,
                                                              'maxlength': 20,
                                                              'placeholder': 'Doe'}))
    email_address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                                  'required': True,
                                                                  'placeholder': 'Email Address',
                                                                  'maxlength': 75}))
    boarding_pass_delivery_type = forms.ChoiceField(choices=DELIVERY_OPTIONS,
                                                    initial='email',
                                                    widget=forms.Select(attrs={'class': 'form-control',
                                                                               'required': True}))
    boarding_pass_deliver_to = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                                             'required': True,
                                                                             'placeholder': 'Email or Phone Number.'}))
    save_on_fare_drops = forms.ChoiceField(choices=SAVINGS_SIGNUP_OPTIONS,
                                           initial='YES',
                                           widget=forms.Select(attrs={'class': 'form-control',
                                                                      'required': True}))

    def clean(self):
        # Capitalize First name and Last name
        try:
            self.cleaned_data['first_name'] = \
                self.cleaned_data['first_name'].strip().capitalize()
        except KeyError as e:
            logger.error('autocheckin form First Name field is empty. ' + str(e))
            raise ValidationError(_("First Name field must not be empty. Try again."))

        try:
            self.cleaned_data['last_name'] = \
                self.cleaned_data['last_name'].strip().capitalize()
        except KeyError as e:
            logger.error('autocheckin form Last Name field is empty. ' + str(e))
            raise ValidationError(_("Last Name field must not be empty. Try again."))

        # Store confirmation number in caps
        try:
            if len(self.cleaned_data['confirmation_number']) != s.CONFIRMATION_NUMBER_LENGTH:
                raise ValidationError(_("Confirmation numbers are 6 characters long."))

            self.cleaned_data['confirmation_number'] = \
                self.cleaned_data['confirmation_number'].upper()
        except KeyError as e:
            logger.error('autocheckin form Confirmation field is empty. ' + str(e))
            raise ValidationError(_("Confirmation field must not be empty. Try again."))

        # Ensure input emails is RFC compliant (external lib)
        try:
            if not validate_email(self.cleaned_data['email_address']):
                raise ValidationError(
                    _("E-mail address does not seem valid (not RFC-compliant). Was it typed correctly?"))
        except KeyError as e:
            logger.error('autocheckin form Email Address field is empty. ' + str(e))
            raise ValidationError(_("Email address field must not be empty. Try again."))

        # Ensure boarding pass delivery email or phone number is valid/possible
        try:
            if 'EMAIL' in self.cleaned_data['boarding_pass_delivery_type']:
                if not validate_email(self.cleaned_data['boarding_pass_deliver_to']):
                    raise ValidationError(
                        _("E-mail address does not seem valid (not RFC-compliant). Was it typed correctly?"))
            elif 'TEXT' in self.cleaned_data['boarding_pass_delivery_type']:
                try:
                    candidate = str(self.cleaned_data['boarding_pass_deliver_to']).replace('-', '')
                    phone = parse(candidate, "US")
                    if not is_possible_number(phone):
                        raise ValidationError(_("Phone number '%s' does not seem valid. Was it typed correctly?" %
                                                self.cleaned_data['boarding_pass_deliver_to']))
                    self.cleaned_data['boarding_pass_deliver_to'] = candidate
                except NumberParseException:
                    raise ValidationError(_("Phone number '%s' does not seem valid. Was it typed correctly?" %
                                            self.cleaned_data['boarding_pass_deliver_to']))
            else:
                logger.error('invalid boarding pass delivery type: %s' %
                             self.cleaned_data['boarding_pass_delivery_type'])
                raise ValidationError(_('unexpected boarding pass delivery type. Aborting.'))
        except KeyError as e:
            logger.error('autocheckin form invalid boarding pass selections. ' + str(e))
            raise ValidationError(_("Invalid boarding pass selections. Try again."))

        # Ensure selection was done on whether or not check for fare drops
        if 'YES' in self.cleaned_data['save_on_fare_drops']:
            self.cleaned_data['save_on_fare_drops'] = True
        elif 'NO' in self.cleaned_data['save_on_fare_drops']:
            self.cleaned_data['save_on_fare_drops'] = False
        else:
            raise ValidationError(_('Must select Yes/No to save money on potential fare drops.'))

        return self.cleaned_data
