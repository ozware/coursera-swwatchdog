from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from encrypted_model_fields.fields import EncryptedCharField

from southwestwatchdog import utils as util

logger = util.initialize_file_logger()


class Customer(models.Model):
    """
    Encapsulates all data tied to a specific Southwest Airlines
    Customer.
    A customer can have multiple Reservations
    """
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email_address = models.CharField(max_length=100)
    account_number = EncryptedCharField(max_length=50,
                                        default="account_number")
    account_password = EncryptedCharField(max_length=40,
                                          default="account_password")

    def __str__(self):
        return '%s %s e-mail: %s\n' % \
               (self.first_name,
                self.last_name,
                self.email_address)


class Feedback(models.Model):
    """
    Contains feedback text and feedback ownership
    for website visitors.
    """
    customer = models.ForeignKey(Customer,
                                 on_delete=models.CASCADE,
                                 blank=True, null=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email_address = models.CharField(max_length=100)
    feedback = models.TextField(default="")
    sent_date = models.DateTimeField(default=None, null=True)


class SpiderStats(models.Model):
    CHECK_IN = 'CI'
    CHANGE_RESERVATION = 'CHR'
    RETRIEVE_RESERVATION = 'RR'
    SPIDER_CHOICES = (
        (CHECK_IN, 'Check In'),
        (CHANGE_RESERVATION, 'Change Reservation'),
        (RETRIEVE_RESERVATION, 'Retrieve Reservation'),
    )

    spider_type = models.CharField(
        max_length=3,
        choices=SPIDER_CHOICES,
        default=RETRIEVE_RESERVATION
    )
    customer = models.ForeignKey(Customer,
                                 on_delete=models.SET_NULL,
                                 null=True)
    confirmation_number = models.CharField(default="",
                                           max_length=6)
    start = models.DateTimeField(default=None)
    end = models.DateTimeField(default=None, null=True, blank=True)
    duration = models.IntegerField(default=None, null=True, blank=True)
    success = models.BooleanField(default=False)
    log = models.TextField(default="")


class CheckInStats(SpiderStats):
    """
    Inherits SpiderStats and defines additional attributes
    specific to only one flight.
    """
    flight_checkin_time = models.DateTimeField(default=None)
    check_in_complete = models.BooleanField(default=False)
    boarding_pass_delivered = models.BooleanField(default=False)
    depart_date = models.DateTimeField(default=None)
    flight_origin = models.CharField(default="",
                                     max_length=30)
    flight_destination = models.CharField(default="",
                                          max_length=30)
    number = models.CharField(default=0, max_length=20)
    boarding_pass_delivery_type = models.CharField(max_length=5)
    boarding_pass_deliver_to = models.CharField(max_length=100)
    flight_way = models.CharField(default="",
                                  max_length=8)


class ChangeReservationStats(SpiderStats):
    """
    Inherits SpiderStats and defines additional attributes
    specific to rebooking a flight.
    """
    saved_on_flight = models.DecimalField(default=0,
                                          decimal_places=2,
                                          max_digits=10)
    outbound_sold_out = models.BooleanField(default=False)
    inbound_sold_out = models.BooleanField(default=False)


class RetrieveReservationStats(SpiderStats):
    pass


class CheckInRunnerStats(models.Model):
    last_run = models.DateTimeField(default=None)
    times_run = models.IntegerField(default=0)
    seconds_since_last_run = models.IntegerField(default=0)


class ReservationError(models.Model):
    """
    Stores details on why retrieving a given reservation
    from southwest.com failed.
    """
    confirmation_number = models.CharField(max_length=6)
    message = models.CharField(max_length=128)


class Reservation(models.Model):
    """
    One Customer can have multiple reservations.
    One Reservation can have multiple Flights (round-trip)
    """
    customer = models.ForeignKey(Customer,
                                 on_delete=models.CASCADE)
    confirmation_number = models.CharField(max_length=6)
    is_round_trip = models.BooleanField(default=False)
    auto_change_flight_on_fare_drop = models.BooleanField(default=True)
    fare_type = models.CharField(max_length=15)

    def __str__(self):
        str_repr = ['\n']
        for key in sorted(self.__dict__):
            str_repr.append('- %s: %s' % (key.replace('_', ' ').title(),
                                          self.__dict__[key]))
        return '\n'.join(str_repr)



class Flight(models.Model):
    """
    Encapsulates all data tied to a specific Southwest Flight
    A Flight can only belong to one Reservation
    """
    # autocheckin app
    reservation = models.ForeignKey(Reservation,
                                    on_delete=models.CASCADE)
    origin_airport = models.CharField(max_length=30, blank=True,
                                      null=True)
    destination_airport = models.CharField(max_length=30,
                                           blank=True,
                                           null=True)
    number = models.CharField(default=0, max_length=20)
    way = models.CharField(max_length=8, default='inbound')
    depart_date = models.DateTimeField(default=None)
    boarding_pass_delivery_type = models.CharField(max_length=5)
    boarding_pass_deliver_to = models.CharField(max_length=100)
    check_in_local_time = models.DateTimeField(default=None)
    check_in_attempts = models.IntegerField(default=0)

    def __str__(self):
        str_repr = ['\n']
        for key in sorted(self.__dict__):
            str_repr.append('- %s: %s' % (key.replace('_', ' ').title(),
                                          self.__dict__[key]))
        return '\n'.join(str_repr)

    def delete(self, *args, **kwargs):
        # Safe to delete Flight now.
        super().delete(*args, **kwargs)
        logger.info('deleted flight with confirmation #%s',
                    self.reservation.confirmation_number)

@receiver(post_delete, sender=Flight)
def _flight_post_delete(sender, instance, **kwargs):
    # Delete the Reservation if it no longer contains any Flights
    flight_number = Flight.objects.filter(
        reservation__confirmation_number__contains=
        instance.reservation.confirmation_number
    ).count()
    if flight_number == 0:
        logger.debug('deleted reservation with confirmation #%s. Last flight already checked into.',
                     instance.reservation.confirmation_number)
        instance.reservation.delete()
