from django.apps import AppConfig


class AutocheckinConfig(AppConfig):
    name = 'autocheckin'
