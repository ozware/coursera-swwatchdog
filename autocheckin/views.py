import json

from django.contrib import auth
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.decorators.csrf import requires_csrf_token, ensure_csrf_cookie

import southwestwatchdog.utils as util
from autocheckin.models import Customer, Reservation, Flight, ReservationError
from southwestwatchdog.spiders.retrievereservation import run_retrieve_reservation_spider
from .forms import AutoCheckinForm

logger = util.initialize_file_logger()


@requires_csrf_token
@login_required()
@ensure_csrf_cookie
def autocheckin(request):
    form = customer = None

    if request.method == 'POST':
        form = AutoCheckinForm(request.POST)
        if form.is_valid():
            # Is this a new or existing Customer?
            customer = handle_existing_customer(form)

            # Is this for a new or existing Reservation?
            data = {'success': handle_existing_flight(customer, request, form),
                    'url': reverse('autocheckin')}
            return JsonResponse(json.dumps(data), safe=False)
    else:
        # Get request. Generate the form.
        form = AutoCheckinForm()

    # Have some manners. Greet the user by First Name.
    first_name = request.user.first_name

    # How many flights are tracked for this user?
    flight_count = len(Flight.objects.filter(
        reservation__customer__email_address__contains=request.user.email
    ))

    return render(request, 'autocheckin/autocheckin.html', {'autocheckin_form': form,
                                                            'name': first_name,
                                                            'flights': flight_count})


def handle_existing_customer(form):
    try:
        customer = Customer.objects.get(
            first_name=form.cleaned_data['first_name'],
            last_name=form.cleaned_data['last_name'],
            email_address=form.cleaned_data['email_address'],
        )
        # Customer exists. Return it.
        return customer
    except ObjectDoesNotExist:
        # Never heard of this person. Instantiate Customer.
        customer = Customer(first_name=form.cleaned_data['first_name'],
                            last_name=form.cleaned_data['last_name'],
                            email_address=form.cleaned_data['email_address'])
        customer.save()
        return customer


def handle_existing_flight(customer, request, form):
    try:
        # Check for existing Reservation matching confirmation number.
        reservation = Reservation.objects.get(
            confirmation_number=form.cleaned_data['confirmation_number']
        )

        # Already tracking this reservation for check-in.
        # Make sure there are Flights in it.
        if not Flight.objects.filter(
                reservation__confirmation_number__contains=form.cleaned_data['confirmation_number']
        ).first():
            # This is an error case. There is a reservation, but no flights for it.
            error = ('Confirmation #%s already tracked in system, but it has no flights '
                     'associated with it. This must be looked into by the Admin.' %
                     form.cleaned_data['confirmation_number'])
            logger.error(error, extra={'user': request.user})
            messages.error(request, error)
            return False

        if reservation.customer != customer:
            # Alert confirmation number is assigned to a different Customer
            message = 'Confirmation #%s reassigned to %s %s.' % \
                      (reservation.confirmation_number,
                       customer.first_name,
                       customer.last_name)
            logger.info(message, extra={'user': request.user})
            messages.info(request, message)
            # Update reservation object
            reservation.customer = customer
            reservation.save()
            return True
        else:
            # It is for the same customer. Nothing to be done.
            message = 'Confirmation #%s already in the system for this passenger.' % \
                      reservation.confirmation_number
            logger.info(message, extra={'user': request.user})
            messages.info(request, message)
            return True
    except ObjectDoesNotExist:
        # Confirmation number isn't in the database
        # Run Scrapy spiders to retrieve the reservation from southwest.com
        run_retrieve_reservation_spider(
            customer=customer,
            confirmation_number=form.cleaned_data['confirmation_number'],
            boarding_pass_delivery_type=form.cleaned_data['boarding_pass_delivery_type'],
            boarding_pass_deliver_to=form.cleaned_data['boarding_pass_deliver_to'],
            auto_change_flight_on_fare_drop=form.cleaned_data['save_on_fare_drops']
        )

        # Check if reservation retrieval was successful
        try:
            res_error = ReservationError.objects.get(
                confirmation_number=form.cleaned_data['confirmation_number']
            )
            messages.add_message(request, messages.ERROR, res_error.message)
            logger.error('reservation retrieval for %s failed. Error: %s,' %
                         (res_error.confirmation_number, res_error.message), extra={'user': request.user})
            res_error.delete()
            return
        except ObjectDoesNotExist:
            # No error detected. Proceed as normal.
            try:
                reservation = Reservation.objects.get(
                    confirmation_number=form.cleaned_data['confirmation_number']
                )
                # Reservation exists now. Scraping was successful.
                message = 'Reservation #%s retrieved from southwest.com and added successfully.' % \
                          reservation.confirmation_number
                messages.success(request, message)
                logger.info(message, extra={'user': request.user})
                return
            except ObjectDoesNotExist:
                error = 'Error processing Reservation #%s. This issue has been reported. Please try again later.' % \
                        form.cleaned_data['confirmation_number']
                messages.error(request, error)
                logger.error(error, extra={'user': request.user})
                return


def logout_view(request):
    auth.logout(request)
    # Redirect to a success page.
    logger.info('%s logged out. Redirecting to login page.',
                request.user, extra={'user': request.user})
    return redirect(reverse("login"))
