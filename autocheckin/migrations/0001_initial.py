# Generated by Django 2.0.4 on 2018-06-26 07:44

from django.db import migrations, models
import django.db.models.deletion
import encrypted_model_fields.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CheckInRunnerStats',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_run', models.DateTimeField(default=None)),
                ('times_run', models.IntegerField(default=0)),
                ('seconds_since_last_run', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('email_address', models.CharField(max_length=100)),
                ('account_number', encrypted_model_fields.fields.EncryptedCharField(default='account_number')),
                ('account_password', encrypted_model_fields.fields.EncryptedCharField(default='account_password')),
            ],
        ),
        migrations.CreateModel(
            name='Feedback',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('email_address', models.CharField(max_length=100)),
                ('feedback', models.TextField(default='')),
                ('sent_date', models.DateTimeField(default=None, null=True)),
                ('customer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='autocheckin.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='Flight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('origin_airport', models.CharField(blank=True, max_length=30, null=True)),
                ('destination_airport', models.CharField(blank=True, max_length=30, null=True)),
                ('number', models.CharField(default=0, max_length=20)),
                ('way', models.CharField(default='inbound', max_length=8)),
                ('depart_date', models.DateTimeField(default=None)),
                ('boarding_pass_delivery_type', models.CharField(max_length=5)),
                ('boarding_pass_deliver_to', models.CharField(max_length=100)),
                ('check_in_local_time', models.DateTimeField(default=None)),
                ('check_in_attempts', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('confirmation_number', models.CharField(max_length=6)),
                ('is_round_trip', models.BooleanField(default=False)),
                ('auto_change_flight_on_fare_drop', models.BooleanField(default=True)),
                ('fare_type', models.CharField(max_length=15)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='autocheckin.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='ReservationError',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('confirmation_number', models.CharField(max_length=6)),
                ('message', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='SpiderStats',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('spider_type', models.CharField(choices=[('CI', 'Check In'), ('CHR', 'Change Reservation'), ('RR', 'Retrieve Reservation')], default='RR', max_length=3)),
                ('confirmation_number', models.CharField(default='', max_length=6)),
                ('start', models.DateTimeField(default=None)),
                ('end', models.DateTimeField(blank=True, default=None, null=True)),
                ('duration', models.IntegerField(blank=True, default=None, null=True)),
                ('success', models.BooleanField(default=False)),
                ('log', models.TextField(default='')),
            ],
        ),
        migrations.CreateModel(
            name='ChangeReservationStats',
            fields=[
                ('spiderstats_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='autocheckin.SpiderStats')),
                ('saved_on_flight', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('outbound_sold_out', models.BooleanField(default=False)),
                ('inbound_sold_out', models.BooleanField(default=False)),
            ],
            bases=('autocheckin.spiderstats',),
        ),
        migrations.CreateModel(
            name='CheckInStats',
            fields=[
                ('spiderstats_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='autocheckin.SpiderStats')),
                ('flight_checkin_time', models.DateTimeField(default=None)),
                ('check_in_complete', models.BooleanField(default=False)),
                ('boarding_pass_delivered', models.BooleanField(default=False)),
                ('depart_date', models.DateTimeField(default=None)),
                ('flight_origin', models.CharField(default='', max_length=30)),
                ('flight_destination', models.CharField(default='', max_length=30)),
                ('number', models.CharField(default=0, max_length=20)),
                ('boarding_pass_delivery_type', models.CharField(max_length=5)),
                ('boarding_pass_deliver_to', models.CharField(max_length=100)),
                ('flight_way', models.CharField(default='', max_length=8)),
            ],
            bases=('autocheckin.spiderstats',),
        ),
        migrations.CreateModel(
            name='RetrieveReservationStats',
            fields=[
                ('spiderstats_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='autocheckin.SpiderStats')),
            ],
            bases=('autocheckin.spiderstats',),
        ),
        migrations.AddField(
            model_name='spiderstats',
            name='customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='autocheckin.Customer'),
        ),
        migrations.AddField(
            model_name='flight',
            name='reservation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='autocheckin.Reservation'),
        ),
    ]
