from django.conf.urls import url

from . import views

urlpatterns = [
    # /autocheckin/
    url(r'^$', views.autocheckin, name='autocheckin'),
]
