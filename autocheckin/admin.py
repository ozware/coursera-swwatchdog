from django.contrib import admin

# Register your models here.
from .models import *


class ReservationAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Reservation._meta.fields]
    ordering = ['customer']


admin.site.register(Reservation, ReservationAdmin)


class FlightAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Flight._meta.fields]
    ordering = ['check_in_local_time']


admin.site.register(Flight, FlightAdmin)


class CheckInStatsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in CheckInStats._meta.fields]
    list_display.remove('log')
    list_display.remove('spiderstats_ptr')
    ordering = ['-flight_checkin_time']


admin.site.register(CheckInStats, CheckInStatsAdmin)


class ChangeReservationStatsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in ChangeReservationStats._meta.fields]
    ordering = ['-start', 'customer']


admin.site.register(ChangeReservationStats, ChangeReservationStatsAdmin)


class RetrieveReservationStatsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in SpiderStats._meta.fields]
    ordering = ['-start', 'customer']


admin.site.register(RetrieveReservationStats, RetrieveReservationStatsAdmin)

admin.site.register(CheckInRunnerStats)
admin.site.register(ReservationError)
