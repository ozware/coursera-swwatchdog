upstream southwestwatchdog_server {
    server unix:/home/azureuser/southwestwatchdog-venv/southwestwatchdog/gunicorn/run/gunicorn.sock fail_timeout=0;
}

# Redirect all non-encrypted to encrypted
#server {
#    server_name swwatchdog.com;
#    listen 80;
#    return 301 https://www.swwatchdog.com$request_uri;
#}

#server {
#    server_name www.swwatchdog.com;
#    listen 80;
#    return 301 https://www.swwatchdog.com$request_uri;
#}

server {
    server_name 104.42.73.153;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl_certificate /etc/nginx/ssl/southwestwatchdog_cert_chain.crt;
    ssl_certificate_key /etc/nginx/ssl/southwestwatchdog.key;

   # Disable SSL and use TLS
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

    # SSL optimizations (connection credential caching)
    ssl_session_cache shared:SSL:20m;
    ssl_session_timeout 180m;

    # Optimize the cipher suites
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DHE+AES128:!ADH:!AECDH:!MD5;

    ssl_dhparam /etc/nginx/cert/dhparam.pem;

    keepalive_timeout 5;
    client_max_body_size 5M;

    access_log /home/azureuser/southwestwatchdog-venv/southwestwatchdog/logs/nginx-access.log;
    error_log /home/azureuser/southwestwatchdog-venv/southwestwatchdog/logs/nginx-error.log;

    location /static/ {
        alias /home/azureuser/southwestwatchdog-venv/southwestwatchdog/static/;
    }

    # if not accessing static files, proxy to app
    location / {
        try_files $uri @proxy_to_app;
    }

    location @proxy_to_app {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	proxy_set_header X-Forwarded-Proto https;
	proxy_set_header Host $http_host;
	proxy_redirect off;
	proxy_pass http://unix:/home/azureuser/southwestwatchdog-venv/southwestwatchdog/gunicorn/run/gunicorn.sock;
    }
}
