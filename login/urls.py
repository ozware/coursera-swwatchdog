from django.conf.urls import url

from login import views as login_views

urlpatterns = [
    # /login/
    url(r'^$', login_views.login, name='login'),
]
