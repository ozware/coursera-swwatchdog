import re

from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from validate_email import validate_email

import southwestwatchdog.django_settings as s
import southwestwatchdog.utils as util

logger = util.initialize_file_logger()


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                             'required': True,
                                                             'placeholder': 'username',
                                                             'maxlength': 20}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                 'required': True,
                                                                 'placeholder': 'password',
                                                                 'maxlength': 20}))
    remember_me = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class': 'login-group-checkbox'}),
                                     label='remember me',
                                     label_suffix="",
                                     required=False)

    def clean(self):
        try:
            if not self.cleaned_data['username']:
                raise ValidationError(_("username field must not be empty."))
        except:
            raise ValidationError(_("username field must not be empty."))

        self.cleaned_data['username'] = \
            self.cleaned_data['username'].strip().lower()

        return self.cleaned_data


class RegisterForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                               'required': True,
                                                               'placeholder': 'First Name'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                              'required': True,
                                                              'placeholder': 'Last Name'}))
    email_address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                                  'required': True,
                                                                  'placeholder': 'Email Address',
                                                                  'maxlength': 75}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                             'required': True,
                                                             'placeholder': 'Username',
                                                             'maxlength': 20}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                 'required': True,
                                                                 'placeholder': 'Password',
                                                                 'maxlength': 20}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                         'required': True,
                                                                         'placeholder': 'Confirm Password',
                                                                         'maxlength': 20}))
    invitation_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                                    'required': True,
                                                                    'placeholder': 'INVITATION CODE',
                                                                    'maxlength': 20}))
    forms.BooleanField(widget=forms.CheckboxInput(attrs={'class': 'login-group-checkbox'}),
                       label='I will not abuse this service.', label_suffix="",
                       required=True)

    def clean(self):
        # Don't bother checking the form if this user isn't authorized.
        self.cleaned_data['invitation_code'] = \
            self.cleaned_data['invitation_code'].lower()

        if self.cleaned_data['invitation_code'] != s.INVITATION_CODE:
            logger.error('invalid registration invitation code input by user: %s',
                         self.cleaned_data['invitation_code'])
            raise ValidationError(_("'%s' is not a valid invitation code." %
                                    self.cleaned_data['invitation_code']))

        # check if username exists in User database
        duplicate_users = User.objects.filter(username=self.cleaned_data['username'])
        if duplicate_users.exists():
            # user exists already with the same username, abort
            raise ValidationError(_("Username '%s' is already registered." %
                                    self.cleaned_data['username']))

        # check if email exists in User database
        duplicate_users = User.objects.filter(email=self.cleaned_data['email_address'])
        if duplicate_users.exists():
            # user exists already with the same username, abort
            raise ValidationError(_("Email '%s' is already registered." %
                                    self.cleaned_data['email_address']))

        # email DNE in database, use external lib to check if it is RFC-compliant
        is_valid = validate_email(self.cleaned_data['email_address'])
        if not is_valid:
            raise ValidationError(_("Email '%s' doesn't seem valid." %
                                    self.cleaned_data['email_address']))

        # validate provided password
        password = self.cleaned_data.get('password')
        x = True
        while x:
            if len(password) > 16:
                break
            elif not re.search("[a-z]", password):
                break
            elif not re.search("[0-9]", password):
                break
            elif not re.search("[A-Z]", password):
                break
            elif re.search("\s", password):
                break
            else:
                x = False
                break

        if x:
            error = "Your password must meet this criteria: \n" \
                    "1) At least 1 letter between [a-z]\n" \
                    "2) At least 1 letter between [A-Z]\n" \
                    "3) At least 1 number between [0-9]\n" \
                    "4) Maximum length 16 characters.\n"
            raise ValidationError(error)

        # verify both passwords typed match
        if self.cleaned_data['password'] != self.cleaned_data['confirm_password']:
            raise ValidationError(_("Passwords do not match. Try again."))

        self.cleaned_data['first_name'] = \
            self.cleaned_data['first_name'].strip().capitalize()
        self.cleaned_data['last_name'] = \
            self.cleaned_data['last_name'].strip().capitalize()
        self.cleaned_data['username'] = \
            self.cleaned_data['username'].strip().lower()

        # we're in business! it's ok to create new User
        return self.cleaned_data


class ForgotPasswordForm(forms.Form):
    email_address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                                  'required': True,
                                                                  'placeholder': 'Email Address',
                                                                  'maxlength': 75}))

    def clean(self):
        forgetful_user = User.objects.filter(email=self.cleaned_data['email_address'])
        if not forgetful_user.exists():
            raise ValidationError(_("Email '%s' is not registered to any user." %
                                    self.cleaned_data['email_address']))

        return self.cleaned_data


class ContactForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                               'required': True,
                                                               'placeholder': 'First Name'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                              'required': True,
                                                              'placeholder': 'Last Name'}))
    email_address = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control',
                                                                   'required': True,
                                                                   'placeholder': 'Email Address'}))
    feedback = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control',
                                                            'required': True,
                                                            'placeholder': 'Type your message here'}))

    def clean(self):
        # use external lib to check if it is RFC-compliant
        if 'email_address' not in self.cleaned_data:
            raise ValidationError(_("Please type a valid email address"))

        is_valid = validate_email(self.cleaned_data['email_address'])
        if not is_valid:
            raise ValidationError(_("Email '%s' does not seem valid" %
                                    self.cleaned_data['email_address']))
        self.cleaned_data['first_name'] = \
            self.cleaned_data['first_name'].strip().capitalize()
        self.cleaned_data['last_name'] = \
            self.cleaned_data['last_name'].strip().capitalize()
        return self.cleaned_data
