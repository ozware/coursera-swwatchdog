from dateutil.tz import tzlocal
import django.contrib.auth as auth
from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.shortcuts import render
from django.template import RequestContext
from django.views.decorators.csrf import requires_csrf_token, ensure_csrf_cookie
import southwestwatchdog.django_settings as s
import southwestwatchdog.utils as util
from .forms import LoginForm, RegisterForm, ContactForm
from autocheckin.models import CheckInStats, Feedback, Customer
from datetime import datetime

logger = util.initialize_file_logger()


def handle_login_form(request):
    logger.info(request.path_info)

    login_form = LoginForm(request.POST, prefix='login')
    if login_form.is_valid():
        # attempt to login user
        username = login_form.cleaned_data['username']
        user = auth.authenticate(username=username,
                                 password=login_form.cleaned_data['password'])
        if not user or not user.is_active:
            messages.error(request, "The username and/or password are incorrect.",
                           extra_tags='login-alert')
            return login_form, redirect(request.path_info)

        auth.login(request, user)
        logger.info('"%s" logged in. Redirecting to %s.'
                    % (user, s.LOGIN_REDIRECT_URL), extra={'user': request.user})
        return login_form, redirect(s.LOGIN_REDIRECT_URL)
    else:
        for error in login_form.non_field_errors():
            messages.error(request, '%s' % error,
                           extra_tags='login-alert')
    return login_form, redirect(request.path_info)


def handle_register_form(request):
    register_form = RegisterForm(request.POST, prefix='register')
    if register_form.is_valid():
        # all fields are valid and are sanitized
        user = User.objects.create_user(username=register_form.cleaned_data['username'],
                                        password=register_form.cleaned_data['password'],
                                        email=register_form.cleaned_data['email_address'],
                                        first_name=register_form.cleaned_data['first_name'],
                                        last_name=register_form.cleaned_data['last_name'])
        # save it in DB
        user.save()
        logger.info('"%s" registered.', user, extra={'user': request.user})
        messages.success(request, "'%s' successfully registered. You may log in now." %
                         user.username, extra_tags='register-alert')
    else:
        for error in register_form.non_field_errors():
            messages.error(request, '%s' % error, extra_tags='register-alert')

    return register_form, redirect(request.path_info)


def handle_contact_form(request):
    customer = None
    contact_form = ContactForm(request.POST, prefix='contact')
    if request.user.is_authenticated:
        customer = Customer.objects.filter(
            first_name=request.user.first_name,
            last_name=request.user.last_name,
            email_address=request.user.email
        )
        if customer:
            customer = customer[0]
            logger.info("%s has provided feedback." % customer)

        first_name = request.user.first_name
        last_name = request.user.last_name
        email_address = request.user.email
        feedback = contact_form['feedback']
    elif contact_form.is_valid():
        first_name = contact_form.cleaned_data['first_name']
        last_name = contact_form.cleaned_data['last_name']
        email_address = contact_form.cleaned_data['email_address']
        feedback = contact_form.cleaned_data['feedback']

        customer = Customer.objects.filter(
            first_name=first_name,
            last_name=last_name,
            email_address=email_address
        )
        if customer:
            customer = customer[0]
            logger.info("linked feedback to %s." % customer)
    else:
        for error in contact_form.non_field_errors():
            messages.error(request, '%s' % error)
        return contact_form, None

    feedback, created = Feedback.objects.get_or_create(
        first_name=first_name,
        last_name=last_name,
        email_address=email_address,
        feedback=feedback,
        sent_date=datetime.now(tzlocal())
    )
    if created and customer:
        feedback.customer = customer
        feedback.save()

    messages.success(request, "Thank you for providing feedback, %s!" % first_name,
                     extra_tags='contact-success')
    return contact_form, redirect(s.CONTACT_REDIRECT_URL)


@requires_csrf_token
@ensure_csrf_cookie
def login(request):
    login_form = register_form = http_redirect = None
    if request.user.is_authenticated:
        logger.info('"%s" is logged in.' % request.user)
    else:
        if request.method == 'POST':
            if 'login' in request.POST.get('action'):
                login_form, http_redirect = handle_login_form(request)
            elif 'register' in request.POST.get('action'):
                register_form, http_redirect = handle_register_form(request)
            elif 'forgot' in request.POST.get('action'):
                # TODO: confirm if email exists and display error/success
                messages.success(request, "If the provided email is valid, you should recieve instructions "\
                                          "on how to reset your password shortly.",
                                 extra_tags='forgotpassword-alert')
                http_redirect = redirect(s.LOGIN_URL)

        if http_redirect:
            return http_redirect

    successful_check_in_count = len(CheckInStats.objects.filter(
        check_in_complete=True
    ))
    if not login_form:
        login_form = LoginForm(prefix='login')
    if not register_form:
        register_form = RegisterForm(prefix='register')
    return render(request, 'login/login.html', {'login_form': login_form,
                                                'register_form': register_form,
                                                'check_in_count': successful_check_in_count})


def contact(request):
    login_form = register_form = contact_form = http_redirect = None
    if request.method == 'POST':
        if 'login' in request.POST.get('action'):
            login_form, http_redirect = handle_login_form(request)
        elif 'register' in request.POST.get('action'):
            register_form, http_redirect = handle_register_form(request)
        elif 'contact' in request.POST.get('action'):
            contact_form, http_redirect = handle_contact_form(request)

    if http_redirect:
        return http_redirect

    if not login_form:
        login_form = LoginForm(prefix='login')
    if not register_form:
        register_form = RegisterForm(prefix='register')
    if not contact_form:
        if request.user.is_authenticated:
            # Pre-fill the form for known users
            contact_form = ContactForm(prefix='contact',
                                       initial={'first_name': request.user.first_name,
                                                'last_name': request.user.last_name,
                                                'email_address': request.user.email})
        else:
            contact_form = ContactForm(prefix='contact')

    return render(request, 'contact/contact.html', {'login_form': login_form,
                                                    'register_form': register_form,
                                                    'contact_form': contact_form})
