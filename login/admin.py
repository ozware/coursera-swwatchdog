# Register your models here.
from django.contrib import admin
from autocheckin.models import *


class CustomerAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Customer._meta.fields]
    ordering = ['last_name']


admin.site.register(Customer, CustomerAdmin)


class FeedbackAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Feedback._meta.fields]


admin.site.register(Feedback, FeedbackAdmin)
