#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "southwestwatchdog.django_settings")
    os.environ.setdefault('DJANGO_CONFIGURATION',
                          os.environ['PRESET_RUN_CONFIG'])

    from configurations.management import execute_from_command_line
    execute_from_command_line(sys.argv)
