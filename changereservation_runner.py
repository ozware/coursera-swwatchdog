import os
import sys
from multiprocessing import Process, Queue

import configurations
import scrapy.crawler as crawler
from scrapy.utils.project import get_project_settings
from tendo import singleton
from twisted.internet import reactor

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "southwestwatchdog.django_settings")
os.environ.setdefault('DJANGO_CONFIGURATION', os.environ['PRESET_RUN_CONFIG'])
configurations.setup()

import southwestwatchdog.django_settings as s
from autocheckin.models import Reservation

os.chdir(s.BASE_DIR)
sys.path.insert(0, s.BASE_DIR)

import southwestwatchdog.utils as util
logger = util.initialize_file_logger()

# Workarounds obscure Selenium/Chrome driver bug
# Refer to: https://bugs.chromium.org/p/chromedriver/issues/detail?id=1552#c43
os.environ["LANG"] = "en_US.UTF-8"

from southwestwatchdog.spiders.changereservation import ChangeReservation


class ChangeReservationRunner:
    def __init__(self):
        def f(q, res):
            try:
                # Ensure required Environment Variables are set
                s.restore_environment_variables()

                '''
                Check if there is enough available RAM before starting spider.
                Unfortunately, Scrapy does not gracefully terminate (crashes)
                if system is low on RAM...
                '''
                if not util.wait_for_enough_ram():
                    # EC2 instance will reboot shortly. Quit the script.
                    return

                runner = crawler.CrawlerRunner(get_project_settings())
                deferred = runner.crawl(ChangeReservation,
                                        reservation=res)
                deferred.addBoth(lambda _: reactor.stop())
                logger.info('(START)\tChange Reservation crawl for: "%s %s %s"',
                            res.customer.first_name,
                            res.customer.last_name,
                            res.confirmation_number)
                reactor.run()
                q.put(None)
            except Exception as e:
                util.report_exception_to_admin(None, e)
                q.put(None)

        # Each Reservation tracked in the system should be checked for
        # potential fare drops.
        q = Queue()

        for reservation in Reservation.objects.all().exclude(fare_type='companion'):
            p = Process(target=f, args=(q,reservation))
            p.start()
            q.get()
            logger.info('(END)\t\tChange Reservation crawl for: "%s %s %s"\n\n',
                        reservation.customer.first_name,
                        reservation.customer.last_name,
                        reservation.confirmation_number)
            p.join()
        return


if __name__ == "__main__":
    if len(sys.argv) != 1:
        # no arguments expected
        logger.error('changereservation_runner.py usage: python changereservation_runner.py (no arguments expected)')
        sys.exit(1)
    else:
        try:
            me = singleton.SingleInstance()
        except singleton.SingleInstanceException:
            logger.info('changereservation_runner.py already running. Exiting.')
            sys.exit(1)

        ChangeReservationRunner()
