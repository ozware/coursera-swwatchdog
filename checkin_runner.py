import os
import sys
from datetime import datetime, timedelta

import configurations
from scrapy.crawler import CrawlerRunner
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.utils import timezone
from scrapy.utils.project import get_project_settings
from tendo import singleton
from twisted.internet import reactor

os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                      "southwestwatchdog.django_settings")
os.environ.setdefault('DJANGO_CONFIGURATION',
                      os.environ['PRESET_RUN_CONFIG'])
configurations.setup()

import southwestwatchdog.django_settings as s
from autocheckin.models import Flight, CheckInRunnerStats

os.chdir(s.BASE_DIR)
sys.path.insert(0, s.BASE_DIR)

import southwestwatchdog.utils as util
logger = util.initialize_file_logger()

# Workarounds obscure Selenium/Chrome driver bug
# Refer to: https://bugs.chromium.org/p/chromedriver/issues/detail?id=1552#c43
os.environ["LANG"] = "en_US.UTF-8"

from southwestwatchdog.spiders.checkin import FlightCheckIn, CheckInStats


def stop_reactor():
    # stop the reactor
    reactor.callFromThread(reactor.stop)
    logger.info('stopped reactor')


if __name__ == "__main__":
    try:
        me = singleton.SingleInstance()
    except singleton.SingleInstanceException:
        logger.info('checkin_runner.py already running. Exiting.')
        sys.exit(1)

    # ensure script is running at desired interval
    current_time = datetime.now(timezone.utc)
    checkinrunner_stats, created = CheckInRunnerStats.objects.get_or_create(
        defaults={'last_run': current_time, 'times_run': 1}
    )
    if created:
        logger.info('CheckInRunnerStats "%s" object created in database.' %
                    checkinrunner_stats)

    time_delta_seconds = timedelta.total_seconds(
        current_time - checkinrunner_stats.last_run
    )
    if time_delta_seconds > s.CHECKIN_RUNNER_RUN_INTERVAL_MINS * 60 +\
            s.CHECKIN_RUNNER_TIME_DELTA_ERROR_SECS:
        error = 'checkin_runner.py last ran %s seconds ago. Last run: %s. Current time: %s' %\
                (int(round(time_delta_seconds)), checkinrunner_stats.last_run, current_time)
        util.report_exception_to_admin(None, error)

    # Update run statistics of checkin_runner_stats.py script
    checkinrunner_stats.last_run = datetime.now(timezone.utc)
    checkinrunner_stats.times_run += 1
    checkinrunner_stats.seconds_since_last_run = time_delta_seconds
    checkinrunner_stats.save()

    '''
    Check if there is enough available RAM before starting spider.
    Unfortunately, Scrapy does not gracefully terminate (crashes)
    if system is low on RAM.
    '''
    if not util.wait_for_enough_ram():
        # EC2 instance will reboot shortly. Quit the script.
        sys.exit("Not enough RAM to run Scrapy Check-In spider")

    for flight in Flight.objects.order_by('check_in_local_time'):
        if not util.is_valid_time_to_check_in(flight):
            # not within <= 24 hr window
            continue

        # Do not spam user. If check-in fails 'CRAWL_RETRY_NUMBER'
        # number of times, then give up on this flight.
        # Both Admin and User have been notified already anyway.
        if flight.check_in_attempts >= s.CRAWL_RETRY_NUMBER:
            logger.info('check-in for "%s" failed %s times already. Skipping.' % \
                        (flight.reservation.confirmation_number, str(flight.check_in_attempts)))
            continue

        # Ensure required Environment Variables are set
        s.restore_environment_variables()

        # Get all flights that have the same check-in time so they
        # can be run simultaneously as opposed to sequentially
        flights = Flight.objects.filter(
            check_in_local_time=flight.check_in_local_time
        )
        if not flights:
            # no need to retry, we are done
            break

        runner = CrawlerRunner(get_project_settings())
        for flight_to_checkin in flights:
            runner.crawl(FlightCheckIn, flight_to_checkin)
        d = runner.join()
        d.addBoth(lambda _: reactor.stop())
        logger.info('(START) Check-In crawl for:%s' % flight)
        reactor.run()  # the script will block here until all crawling jobs are finished

        for flight_to_checkin in flights:
            # Verify check in was successful or retry
            try:
                checkin_stats = CheckInStats.objects.get(
                    spider_type='CI',
                    confirmation_number=flight.reservation.confirmation_number,
                    flight_origin=flight.origin_airport,
                    flight_destination=flight.destination_airport,
                    number=flight.number,
                    depart_date=flight.depart_date,
                    flight_checkin_time=flight.check_in_local_time,
                    flight_way=flight.way,
                    check_in_complete=True
                )
            except MultipleObjectsReturned:
                error = 'check-in was performed multiple times for "%s"' %\
                        flight.reservation.confirmation_number
                util.report_exception_to_admin(None, error)
            except ObjectDoesNotExist:
                error = 'no CheckInStats object was found for "%s"' %\
                        flight.reservation.confirmation_number
                util.report_exception_to_admin(None, error)
            else:
                if checkin_stats.check_in_complete and \
                        checkin_stats.boarding_pass_delivered:
                    logger.info('(END) Check-In crawl: SUCCESS')
                    continue
                elif checkin_stats.check_in_complete:
                    logger.info('(END) Check-In crawl: Check-In Succesful, Boarding Pass delivery failed.')
                else:
                    logger.info('(END) Check-In crawl: FAILURE')
                flight.check_in_attempts += 1
                flight.save()
