1) Gunicorn/Nginx/supervisord tutorial:
   https://simpleisbetterthancomplex.com/tutorial/2017/05/23/how-to-deploy-a-django-application-on-rhel.html
   https://codingstartups.com/deploy-django-nginx-gunicorn-postgresql-supervisor/

2) After installing supervisord, 'mv southwestwatchdog.conf /etc/supervisor/conf.d/'

3) To start supervisord for the first time:
   cd /home/ubuntu/southwestwatchdog-vnv/bin
   sudo supervisord -c /etc/supervisor/conf.d/southwestwatchdog.conf
   sudo supervisorctl -c /etc/supervisor/conf.d/southwestwatchdog.conf reload
   sudo supervisorctl -c /etc/supervisor/conf.d/southwestwatchdog.conf start southwestwatchdog

4) To restart supervisord/nginx:
alias swwdeploy="cd /home/ubuntu/southwestwatchdog-vnv/bin && sudo supervisorctl -c /etc/supervisor/conf.d/southwestwatchdog.conf reread && sudo supervisorctl -c /etc/supervisor/conf.d/southwestwatchdog.conf update && sudo supervisorctl -c /etc/supervisor/conf.d/southwestwatchdog.conf restart southwestwatchdog && sudo service nginx restart"
