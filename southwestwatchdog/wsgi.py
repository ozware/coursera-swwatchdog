import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      os.environ['DJANGO_SETTINGS_MODULE'])
os.environ.setdefault('DJANGO_CONFIGURATION',
                      os.environ['PRESET_RUN_CONFIG'])

from configurations.wsgi import get_wsgi_application

application = get_wsgi_application()
