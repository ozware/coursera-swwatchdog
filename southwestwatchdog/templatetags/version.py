from django import template

import southwestwatchdog.django_settings as s

register = template.Library()

@register.simple_tag()
def last_modified():
    return s.LAST_MODIFIED