# -*- coding: utf-8 -*-

import scrapy


class ScrapyFlight(scrapy.Item):
    # autocheckin app
    confirmation_number = scrapy.Field()  # done
    origin_airport = scrapy.Field()
    destination_airport = scrapy.Field()
    number = scrapy.Field()
    way = scrapy.Field()  # 'outbound' or 'inbound'
    depart_date = scrapy.Field()
    boarding_pass_delivery_type = scrapy.Field()  # done
    boarding_pass_deliver_to = scrapy.Field()  # done
    check_in_local_time = scrapy.Field()

    def __str__(self):
        str_repr = ['\n']
        for key in sorted(self.__dict__['_values']):
            str_repr.append('- %s: %s' % (key.replace('_', ' ').title(),
                                          self.__dict__['_values'][key]))
        return '\n'.join(str_repr)


class ScrapyReservation(scrapy.Item):
    # autocheckin app
    customer = scrapy.Field()  # done
    confirmation_number = scrapy.Field()  # done
    is_round_trip = scrapy.Field()  # done
    auto_change_flight_on_fare_drop = scrapy.Field()
    fare_type = scrapy.Field()
    scraping_error = scrapy.Field()  # done

    def __str__(self):
        str_repr = ['\n']
        for key in sorted(self.__dict__['_values']):
            str_repr.append('- %s: %s' % (key.replace('_', ' ').title(),
                                          self.__dict__['_values'][key]))
        return '\n'.join(str_repr)