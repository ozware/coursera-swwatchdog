"""southwestwatchdog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.http import HttpResponse

from autocheckin import views as autocheckin_views
from login import views as login_views

urlpatterns = [
    url(r'^robots.txt', lambda x: HttpResponse("User-Agent: *\nDisallow:", content_type="text/plain"),
        name="robots_file"),
    url(r'^admin/', admin.site.urls),
    url(r'^$', login_views.login, name='login'),
    url(r'^login/$', login_views.login, name='login'),
    url(r'^contact/', login_views.contact, name='contact'),
    url(r'^autocheckin/', autocheckin_views.autocheckin, name='autocheckin'),
    url(r'^logout/', auth_views.logout, {'next_page': '/login/'}, name='logout'),

    url(r'^login/password_reset/$',
        auth_views.password_reset,
        {'post_reset_redirect': '/login/password_reset/done/'},
        name='password_reset'),
    url(r'^login/password_reset/done/$',
        login_views.login,
        name='password_reset_done'),
    url(r'^login/password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        {'post_reset_redirect': '/login/password_reset/complete/'},
        name='password_reset_confirm'),
    url(r'^login/password_reset/complete/$',
        auth_views.password_reset_complete,
        name='password_reset_complete'),
]

urlpatterns += staticfiles_urlpatterns()
