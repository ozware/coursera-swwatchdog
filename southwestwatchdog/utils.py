import inspect
import logging
import os
import re
import sys
import time
import traceback
from datetime import datetime, timedelta
from inspect import currentframe, getframeinfo
from io import StringIO
from smtplib import SMTPRecipientsRefused

import subprocess
import boto3
import botocore.exceptions
import coloredlogs
import pytz
import scrapy.exceptions
from django.core.mail import send_mail as mail
from django.utils import timezone
from geopy.exc import GeocoderTimedOut
from geopy.geocoders import GoogleV3
from geopy.geocoders import Nominatim
from pyvirtualdisplay import Display
from scrapy import Spider
from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from timezonefinder import TimezoneFinder

import southwestwatchdog.django_settings as s


def initialize_file_logger():
    custom_logger = logging.getLogger(s.PROJECT_NAME)
    if not len(custom_logger.handlers):
        hdlr = logging.FileHandler(s.LOG_PATH)
        formatter = logging.Formatter(fmt=s.LOG_FORMAT,
                                      datefmt=s.DATE_FORMAT)
        hdlr.setFormatter(formatter)
        custom_logger.addHandler(hdlr)
        custom_logger.setLevel(s.LOG_LEVEL)
        coloredlogs.install(level=s.LOG_LEVEL,
                            fmt=s.LOG_FORMAT,
                            logger=custom_logger,
                            field_styles=s.FIELD_STYLES)
    return custom_logger


# log to entire project
logger = initialize_file_logger()


def initialize_stringio_logger(self):
    """
    Initializes a stream log as a Stream IO object and
    the logger for the object passed in as argument. Stream
    log makes it possible to get the entire log as a single
    string.
    """
    try:
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.setLevel(s.LOG_LEVEL)
        self.log_stream = StringIO()
        ch = logging.StreamHandler(self.log_stream)
        ch.setLevel(s.LOG_LEVEL)
        ch.setFormatter(logging.Formatter(fmt=s.LOG_FORMAT,
                                          datefmt=s.DATE_FORMAT))
        self.log.addHandler(ch)
    except Exception as e:
        error = 'failed to set up log stream for %s.\n%s' % (self, str(e))
        report_exception_to_admin(None, error)


def available_memory():
    """
    Get estimate of how much memory is available for starting
    new applications in MB.
    """
    try:
        with open('/proc/meminfo', 'r') as mem:
            ret = {}
            tmp = 0
            for i in mem:
                sline = i.split()
                if 'MemAvailable' in str(sline[0]):
                    ret['available'] = round(int(sline[1]) / 1024)
        return ret['available']
    except (OSError, IOError):
        logger.error('/proc/meminfo was not found. Are you running on an EC2 instance?')
        return False


def reboot_ec2_instance(available_memory):
    if 'INSTANCE_ID' in os.environ:
        # Running on AWS
        ec2 = None
        try:
            ec2 = boto3.client('ec2')
            ec2.reboot_instances(InstanceIds=[os.environ['INSTANCE_ID']],
                                 DryRun=True)
        except botocore.exceptions.ClientError as e:
            if 'DryRunOperation' not in str(e):
                logger.error("You don't have permission to reboot instances")
                return False
        except:
            logger.error('unable to get EC2 client and reboot it. Are you running on an EC2 host?')
            return False

        # Announce it.
        subject = 'AWS EC2 instance rebooted due to low RAM'
        message = 'EC2 instance has %s MB of RAM available which is less than the configured ' \
                  'threshold needed for crawling spider runs.'
        send_mail(subject, message, s.EMAIL_HOST_ADMIN)

        # Actual reboot
        try:
            response = ec2.reboot_instances(InstanceIds=[os.environ['INSTANCE_ID']],
                                            DryRun=False)
            logger.info('success sending EC2 reboot command: %s', str(response))
        except botocore.exceptions.ClientError as e:
            logger.error(str(e))
    else:
        # Announce it.
        subject = 'Azure VM instance rebooted due to low RAM'
        message = 'Azure VM instance has %s MB of RAM available which is less than the configured ' \
                  'threshold needed for crawling spider runs.' % available_memory
        send_mail(subject, message, s.EMAIL_HOST_ADMIN)

        # Running on Azure
        vm_id = subprocess.check_output(['bash', '-c', 'az vm list --query [].id --output tsv'])
        restart_command=b'az vm restart --ids %s"' % vm_id
        output = subprocess.check_output(['bash', '-c', restart_command])

def wait_for_enough_ram():
    tries = s.CHECKIN_SPIDER_RAM_CHECK_RETRY
    while tries > 0:
        available_mem = available_memory()
        if not available_mem:
            # We don't know who much RAM is avaiable. Let it run.
            print("/proc/mem not available - unable to check free RAM")
            return True

        if available_mem < s.CHECKIN_SPIDER_RAM_REQUIREMENT:
            logger.error('only %s MB of available RAM. Waiting %s seconds...',
                         available_mem, s.CHECKIN_SPIDER_AVAILABLE_RAM_DELAY)
            time.sleep(s.CHECKIN_SPIDER_AVAILABLE_RAM_DELAY)
        else:
            logger.debug('%s MB of available RAM. Spider can run now.',
                        available_mem)
            return True
        tries -= 1

    if tries == 0:
        logger.critical('not enough RAM to run spider after %s retries. Rebooting machine.',
                        s.CHECKIN_SPIDER_RAM_CHECK_RETRY)
        reboot_ec2_instance(available_mem)
        return False


def get_method_name():
    return inspect.stack()[1][3]


def send_mail(subject, message, recipient):
    try:
        mail(subject=subject,
             message=message,
             from_email=s.EMAIL_HOST_USER,
             recipient_list=[recipient])
    except SMTPRecipientsRefused:
        logger.error("unable to send email.%s\n" % traceback.format_exc())
    except Exception as e:
        logger.error(str(e))


def report_exception_to_admin(self, error):
    if error:
        subject = "Exception Report"
        message = str(error) + \
                  "\n\nStack Trace:\n\n%s" % \
                  ''.join(traceback.format_stack(limit=7)[:-1])

        if self and self.log:
            self.log.error(message)
        else:
            logger.error(message)
        send_mail(subject, message, s.EMAIL_HOST_USER)


def open_webdriver(self):
    try:
        if s.USE_PYVIRTUAL_DISPLAY:
            sys.path.append(s.XVBF_BINARY_PATH)
            #Open Pyvirtualdisplay
            self.display = Display(visible=0, size=(1200, 800),
                                   use_xauth=True)
            self.display.start()

        options = webdriver.ChromeOptions()
        options.add_argument('--start-maximized')
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('--disable-extensions')
        options.add_argument('--incognito')

        service_options = ['--verbose']
        service_log = s.CHROME_DRIVER_LOG_PATH

        # Open 'Retrieve Air Reservation' page of southwest.com on WebDriver
        if s.DEBUG:
            self.driver = webdriver.Chrome(
                executable_path=s.CHROME_DRIVER_PATH,
                chrome_options=options,
                service_args=service_options,
                service_log_path=service_log
            )
        else:
            self.driver = webdriver.Chrome(
                executable_path=s.CHROME_DRIVER_PATH,
                chrome_options=options,
            )
        self.driver.set_script_timeout(30)
        self.driver.set_page_load_timeout(30)  # seconds
        self.log.debug('Chrome WebDriver initialization done. Opening URL: "%s".' %
                       self.start_urls[0])
        self.driver.get(self.start_urls[0])
    except:
        error = 'failed to open %s on WebDriver.\n%s' % \
                (self.start_urls[0], traceback.format_exc())
        abort_spider_crawl(self, error)


def close_webdriver(self):
    try:
        if self.driver:
            self.driver.quit()
        if self.display and s.USE_PYVIRTUAL_DISPLAY:
            self.display.stop()
    except:
        error = 'failed to close WebDriver and PyVirtualDisplay.\n%s' % \
                (traceback.format_exc())
        abort_spider_crawl(self, error)


def abort_spider_crawl_visible(self, error):
    from autocheckin.models import ReservationError

    reservation_error = ReservationError(
        confirmation_number=self.confirmation_number,
        message=error
    )

    # Allows front-end to display human-readable error on why
    # retrieving a reservation failed.
    reservation_error.save()
    abort_spider_crawl(self, error)


def abort_spider_crawl(self, error):
    report_exception_to_admin(self, str(error))

    if hasattr(self, 'reservation_item') \
            and self.reservation_item:
        self.reservation_item['scraping_error'] = True

    if self.driver:
        self.driver.quit()

    # Update the Stats object associated with the crawl
    save_checkin_run_stats(self, False)

    raise scrapy.exceptions.CloseSpider()


def is_dst(tz_name):
    now = datetime.now(tz=pytz.timezone(tz_name))
    dst_timedelta = now.dst()
    ### dst_timedelta is offset to the winter time,
    ### thus timedelta(0) for winter time and timedelta(0, 3600) for DST;
    ### it returns None if timezone is not set
    return True if dst_timedelta else None


def localize_datetime(self, target_city_state, dt_object):
    """
    Adjusts a given datetime to a new timezone which matches
    the origin city. "target_city_state" string will be regex matched for:
    'City, State'"
    """
    google_success = False
    for attempt in range(3):
        # Let's try a few times. We rely on external APIs that
        # could lag...
        try:
            # Standardize flight departure time to UTC:
            # 'City, State' -> 'Lat, Long' -> 'Time Zone'
            if re.findall(r'(.*\,.*)\-',
                                    target_city_state,
                                    re.I | re.M):
                city_state = re.findall(r'(.*\,.*)\-',
                                    target_city_state,
                                    re.I | re.M)[0].strip()
            else:
                city_state = re.findall(r'(.*\,.*)',
                                        target_city_state,
                                        re.I | re.M)[0].strip()
            g = GoogleV3(api_key=s.GOOGLE_GEOCODER_API_KEY)
            place, (lat, lng) = g.geocode(city_state)
            if lat and lng:
                google_success = True
                break
        except GeocoderTimedOut:
            logger.info('GeocoderTimedOut, reattempt: %s', attempt)
            continue
        except:
            logger.warn('unexpected exception using Google Gelocation service.\n%s' % \
                        traceback.format_exc())
    try:
        if not google_success:
            # Something went wrong with GoogleV3 geolocator. Try Nominatim.
            g = Nominatim(scheme="http")
            location = g.geocode(city_state, timeout=10)
            if not location:
                # both google/nominatim geocoder failed. Abort.
                error = ('Geocoder APIs failed.%s\n%s' % (traceback.format_exc(), self))
                abort_spider_crawl(self, error)

            lat = location.latitude
            lng = location.longitude
    except:
        error = ('%s\n%s' % (traceback.format_exc(), self))
        abort_spider_crawl(self, error)

    try:
        tf = TimezoneFinder()
        target_tz = tf.timezone_at(lat=lat, lng=lng)

        if dt_object.tzinfo is not None \
                and dt_object.tzinfo.utcoffset(dt_object) is not None:
            # Timezone aware datetime
            result = dt_object.astimezone(pytz.timezone(target_tz))
            self.log.debug('timezone aware date: %s -> %s' % \
                           (dt_object, result))
        else:
            # Naive datetime
            # localize() takes a naive datetime object and interprets
            # it as if it is in that timezone.
            result = pytz.timezone(
                target_tz
            ).localize(
                dt_object,
                is_dst=is_dst(target_tz)
            )
            logger.debug('naive date: %s -> %s' % \
                         (dt_object, result))
        return result
    except:
        error = ('%s\n%s' % (traceback.format_exc(), self))
        abort_spider_crawl(self, error)


def get_element_by(self, lookup_string, by, fatal):
    try:
        switcher = {
            By.CSS_SELECTOR: self.driver.find_element_by_css_selector,
            By.ID: self.driver.find_element_by_id,
            By.CLASS_NAME: self.driver.find_element_by_class_name,
            By.XPATH: self.driver.find_element_by_xpath,
            By.ID: self.driver.find_element_by_id
        }
        wait = WebDriverWait(self.driver, s.ELEMENT_LOAD_TIMEOUT)
        wait.until(EC.presence_of_element_located(
            (by, lookup_string))
        )
    except TimeoutException:
        error = 'timed out waiting %s seconds for element "%s".\n%s' % \
                (s.ELEMENT_LOAD_TIMEOUT, lookup_string, traceback.format_exc())
        if fatal:
            abort_spider_crawl(self, error)
        else:
            return False

    try:
        func = switcher.get(by, lambda : "Invalid By Value")
        return func(lookup_string)
    except NoSuchElementException:
        # Web page design could change at anytime. Log.
        error = 'failed to find element "%s".\n%s' % \
                (lookup_string, traceback.format_exc())
        abort_spider_crawl(self, error)
    return


def get_elements_by(self, lookup_string, by, fatal):
    switcher = {
        By.CSS_SELECTOR : self.driver.find_elements_by_css_selector,
        By.ID : self.driver.find_elements_by_id,
        By.CLASS_NAME : self.driver.find_elements_by_class_name,
        By.XPATH : self.driver.find_elements_by_xpath,
        By.ID : self.driver.find_elements_by_id
    }

    try:
        wait = WebDriverWait(self.driver, s.ELEMENT_LOAD_TIMEOUT)
        wait.until(EC.presence_of_all_elements_located(
            (by, lookup_string))
        )
        time.sleep(0.5)
    except TimeoutException:
        error = 'timed out waiting %s seconds for element "%s".\n%s' % \
                (s.ELEMENT_LOAD_TIMEOUT, lookup_string, traceback.format_exc())
        if fatal:
            abort_spider_crawl(self, error)
        else:
            return False

    try:
        func = switcher.get(by, lambda : "Invalid By Value")
        return func(lookup_string)
    except NoSuchElementException:
        # Web page design could change at anytime. Log.
        error = 'failed to find element "%s".\n%s' % \
                (lookup_string, traceback.format_exc())
        abort_spider_crawl(self, error)
    return


def input_text_to_element_by(self, lookup_string, text, by):
    element = get_element_by(
        self,
        lookup_string,
        by,
        True
    )
    try:
        click_element(self, element)
        time.sleep(0.5)
        element.clear()
    except:
        element.send_keys(Keys.DELETE)

    time.sleep(0.5)
    element.send_keys(text)
    time.sleep(0.5)
    # Handle auto-complete forms
    element.send_keys(Keys.TAB)
    return element


def click_element(self, element):
    click_successful = False
    try:
        element.click()
        click_successful = True
    except WebDriverException:
        pass

    try:
        try:
            if not click_successful:
                self.log.debug('failed to click on element "%s", fall back on enter' %
                             (element.get_attribute('class')))
                # Known bug on chromedriver. Try the Enter key on the button.
                element.send_keys(Keys.RETURN)
        except (NoSuchElementException, StaleElementReferenceException) as e:
            self.log.warn('"%s" was stale. Continuing.' % element.get_attribute('class'))
    except WebDriverException:
        error = 'could not press enter key on element "%s"' % \
                element.get_attribute('class')
        abort_spider_crawl(self, error)


def click_element_by(self, lookup_string, by):
    element = get_element_by(
        self,
        lookup_string,
        by,
        True
    )
    click_element(self, element)


def click_elements_by(self, lookup_string, by):
    counter = 0
    for element in get_elements_by(
            self,
            lookup_string,
            by,
            True
    ):
        click_element(self, element)
        counter += 1
    return counter


def get_linenumber():
    cf = currentframe()
    return cf.f_back.f_lineno


def format_error(error):
    cf = currentframe()
    filename = getframeinfo(cf).filename
    return "%s:%s ERROR: %s" % (filename, cf.f_back.f_lineno, error)


def regex_find_match(self, string, pattern):
    match = re.findall(pattern, string, re.I | re.M)
    if match:
        return match[0].strip()
    else:
        error = "regex pattern: '%s' not found in string: '%s'" % \
                (pattern, string)
        abort_spider_crawl(self, error)


def is_valid_time_to_check_in(self):
    """
    Based on local server time, determines if an existing
    Southwest Flight can be checked into.
    """
    try:
        spider_object = isinstance(self, Spider)
        flight = local_logger = None
        if spider_object:
            flight = self.flight
            local_logger = self.log
        else:
            flight = self
            local_logger = logger

        now = datetime.now(timezone.utc)
        if now >= flight.check_in_local_time:
            local_logger.debug('current time: %s  >= check-in time: %s' %
                               (now, flight.check_in_local_time))
            return True
        else:
            # If we are within a minute away, block until it is time
            delta_seconds = (flight.check_in_local_time - now).total_seconds() + 1
            if delta_seconds < s.CHECK_IN_TOLERANCE_ERROR_SECONDS:
                if spider_object:
                    local_logger.info('blocking for %s seconds' % delta_seconds)
                    time.sleep(delta_seconds)
                    local_logger.info('continue...')
                else:
                    local_logger.info('continuing check-in process, but Check-In spider will need to wait %s s' %
                                      delta_seconds)
                return True
            else:
                reason = ('%s %s %s %s: check-in will occur in: %s s. Check-in at: %s.' %
                          (flight.reservation.customer.first_name,
                           flight.reservation.customer.last_name,
                           flight.origin_airport,
                           flight.destination_airport,
                           int(delta_seconds),
                           flight.check_in_local_time))
                if delta_seconds <= s.CHECK_IN_DISPLAY_NEXT_CHECK_IN_THRESHOLD:
                    local_logger.info(reason)
                return False
    except Exception as e:
        report_exception_to_admin(self, e)
        return False


def save_checkin_run_stats(self, success):
    """
    Updates a 'SpiderStats' object attributes when an exception is
    thrown or at the end of a spider crawl. The following
    attributes are updated: 'end', 'duration', 'success',
    and 'log'.
    :return:
    """
    try:
        self.stats.end = datetime.now(timezone.utc)
        self.stats.duration = timedelta.total_seconds(
            self.stats.end - self.stats.start
        )
        self.stats.success = success
        if not self.log_stream or self.log_stream.closed:
            self.stats.log = "Log stream not initialized yet"
        else:
            self.stats.log = self.log_stream.getvalue()
            self.log_stream.close()
        self.stats.save()
    except Exception as e:
        error = 'exception saving statistics for %s.\n%s' %\
                (self, str(e))
        report_exception_to_admin(None, error)
