# -*- coding: utf-8 -*-
import datetime
import time
import traceback

from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.utils import timezone
from scrapy.exceptions import DropItem

import southwestwatchdog.utils as util
from autocheckin.models import Reservation, Flight
from southwestwatchdog.items import ScrapyFlight
from southwestwatchdog.items import ScrapyReservation

logger = util.initialize_file_logger()


class ReservationPipeline(object):
    def process_item(self, item, spider):
        if not isinstance(item, ScrapyReservation):
            return item

        logger.debug('start %s:%s().%s',
                     self.__class__.__name__,
                     util.get_method_name(),
                     str(item))

        if item.get('scraping_error'):
            # Something went wrong scraping this res. Drop it.
            logger.error('Scraping error on Reservation Item: %s. Dropped.' % item)
            raise DropItem()
        else:
            # Reservation is good. Store it in DB.
            try:
                reservation, created = Reservation.objects.get_or_create(
                    customer=item['customer'],
                    confirmation_number=item['confirmation_number'],
                    is_round_trip=item['is_round_trip'],
                    fare_type=item['fare_type'],
                    auto_change_flight_on_fare_drop=item['auto_change_flight_on_fare_drop']
                )
                if created:
                    logger.info('successfully saved database object:%s',
                                reservation)
                else:
                    logger.info('database object already exists:%s',
                                reservation)

                logger.debug('end %s:%s()\n',
                             self.__class__.__name__,
                             util.get_method_name())

                return item
            except:
                logger.error('unexpected error saving database object:\n%s.\n%s',
                             item, traceback.format_exc())
                raise DropItem()


class FlightPipeline(object):
    def process_item(self, flight, spider):
        if not isinstance(flight, ScrapyFlight):
            return flight

        logger.debug('start %s:%s().%s',
                     self.__class__.__name__,
                     util.get_method_name(),
                     flight)

        reservation_db = None

        for i in range(0, 15):
            try:
                reservation_db = Reservation.objects.get(
                    confirmation_number=flight['confirmation_number']
                )
                time.sleep(1)
            except ObjectDoesNotExist:
                continue
            except MultipleObjectsReturned:
                logger.error('multiple Reservation objects found for confirmation #%s',
                             flight['confirmation_number'])
                raise DropItem()
            break

        if not reservation_db:
            logger.error('Reservation object for confirmation #%s not found.',
                         flight['confirmation_number'])
            raise DropItem()

        try:
            # Is this Flight in the past?
            if datetime.datetime.now(timezone.utc) > \
                    flight['depart_date']:
                logger.info('flight departure is in the past. Ignoring %s flight in #%s',
                            flight['way'],
                            flight['confirmation_number'])
                return flight

            # Create new Flight object and store it in DB.
            flight, created = Flight.objects.get_or_create(
                reservation=reservation_db,
                depart_date=flight['depart_date'],
                boarding_pass_delivery_type=flight['boarding_pass_delivery_type'],
                boarding_pass_deliver_to=flight['boarding_pass_deliver_to'],
                check_in_local_time=flight['check_in_local_time'],
                way=flight['way'],
                origin_airport=flight['origin_airport'],
                destination_airport=flight['destination_airport'],
                number=flight['number'])
            if created:
                logger.info('successfully saved database object:%s', flight)
            else:
                logger.info('database object already exists:%s',
                            flight)

            logger.debug('end %s:%s()\n',
                         self.__class__.__name__,
                         util.get_method_name())
            return flight
        except:
            logger.error('unexpected error saving database object:%s.\n%s',
                         flight, traceback.format_exc())
            raise DropItem()
